import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
class Dialogobase(Gtk.Window):
    def __init__(self, parent):
        super().__init__(title="¡¡Atención!!")
        caja = Gtk.Box()
        label0 = Gtk.Label(label="Estos son los datos que serán guardados")
        label1 = Gtk.Label()
        label2 = Gtk.Label()
        boton0 = Gtk.Button(label="abrir carpeta")
        
        contrasena = "contraseña: "+parent.entrypassword.get_text()
        usuario = "usuario: "+parent.entryusuario.get_text()
        label1.set_text(usuario)
        label2.set_text(contrasena)
        
        caja.pack_start(label0, True, True, 0)
        caja.pack_start(label1, True, True, 0)
        caja.pack_start(label2, True, True, 0)
        caja.pack_start(boton0, True, True, 0)
        self.add(caja)
        
class Principal(Gtk.Window):
    def __init__(self):
        super().__init__(title="Programacion avanzada")
        
        counteinermaine = Gtk.Box()
        counteinerPri = Gtk.Box()
        counteinerSec = Gtk.Box()
        counteinerTer = Gtk.Box()
        
        self.entryusuario = Gtk.Entry()
        self.entrypassword = Gtk.Entry()
        
        generarpassword = Gtk.Button()
        generarpassword.connect("clicked", self.password)
        confirmarpassword = Gtk.Button()
        self.confirmarpassword.connect("clicked", self.confirmarpassword)
        
        counteinerSec.setorientation(0)
        
        counteinerPri.pack_start(self.entryusuario, True, True, 0)
        
        counteinerSec.pack_start(self.entrypassword, True, True, 0)
        counteinerSec.pack_start(generarpassword, True, True, 0)
        
        counteinerTer.pack_start(confirmarpassword, True, True, 0)
        
        counteinermaine.pack_star(counteinerPri, True, True, 0)
        counteinermaine.pack_star(counteinerSec, True, True, 0)
        counteinermaine.pack_star(counteinerTer, True, True, 0)
        self.add(counteinermaine)
        
    def password(self, widget):
        self.entrypassword.set_text("Contraseña Generada")
        
    def confirmarpassword(self, widget):
        dialogo = Dialogobase(self)
        dialogo.show_all()
    
     
ventana = Principal()
ventana.connect("destroy",Gtk.main_quit)
ventana.show_all()
Gtk.main()